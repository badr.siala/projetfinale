import calculator


def test_addition():
    assert calculator.add(4, 3) == 7


def test_subtraction():
    assert calculator.subtract(7, 2) == 5


def test_multiplication():
    assert calculator.multiply(2, 8) == 16
